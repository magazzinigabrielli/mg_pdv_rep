CREATE OR REPLACE PACKAGE FRAME."BASE_LOG_PCK" AS
  --
  /******************************************************************************
   NAME:       BASE_LOG_PCK
   PURPOSE:    Package di logging

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        12/02/2019           r.doti       1. Created this package.
   ******************************************************************************/
  --
  --
  -- Livelli di trace
  Errore        CONSTANT  VARCHAR2(1) := 'E';
  Warning       CONSTANT  VARCHAR2(1) := 'W';
  Info          CONSTANT  VARCHAR2(1) := 'I';
  DEBUG         CONSTANT  VARCHAR2(1) := 'D';
  --
  -- ===================================================================
  -- Procedura per scrittura messaggi di log in tabella BASE_LOG
  --LvlTrace=1 AND  pLOG_TIPO ='E') OR
  --       (LvlTrace=2 AND (pLOG_TIPO ='E'  OR pLOG_TIPO ='W')) OR
  --       (LvlTrace=3 AND (pLOG_TIPO ='E'  OR pLOG_TIPO ='W'   OR  pLOG_TIPO ='I')) OR
  --      (LvlTrace=4 AND (pLOG_TIPO ='E'  OR pLOG_TIPO ='W'   OR  pLOG_TIPO ='I'   OR pLOG_TIPO ='D'))
  -- Parametri di input :
  -- 2 - pID              : Nome Package
  -- 3 - pLOG_MSG         : Testo del Messagigo
  -- 4 - pLOG_TIPO        : Tipo del Messaggio (I,E,W,D)
  --
  -- ===================================================================
  PROCEDURE WriteLog(
   pLOG_PKG          IN BASE_LOG.LOG_PKG%TYPE,
   pLOG_MSG          IN BASE_LOG.LOG_MSG%TYPE,
   pLOG_TIPO         IN BASE_LOG.LOG_TIPO%TYPE
  );
  -- =================================================
  -- Restituisce la session id creata in fase di
  -- inizializzazione
  --
  -- Return: session ID
  -- =================================================
  FUNCTION GetSessionId RETURN BASE_LOG.LOG_ID_SESSIONE%TYPE;
--
END BASE_LOG_PCK;
/
CREATE OR REPLACE PUBLIC SYNONYM BASE_LOG_PCK FOR FRAME."BASE_LOG_PCK"
/
GRANT EXECUTE ON BASE_LOG_PCK TO MG
/
