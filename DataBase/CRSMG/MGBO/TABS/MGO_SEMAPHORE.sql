--------------------------------------------------------
--  File creato - venerd�-aprile-02-2021   
--------------------------------------------------------
DROP TABLE "MGBO"."MGO_SEMAPHORE";
--------------------------------------------------------
--  DDL for Table MGO_SEMAPHORE
--------------------------------------------------------

  CREATE TABLE "MGBO"."MGO_SEMAPHORE" 
   (	"SEM_STATUS" CHAR(1 BYTE), 
	"SEM_LAST_CHANGE_DATE" TIMESTAMP (6), 
	"SEM_TYPE" VARCHAR2(20 BYTE)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "MGBO_TBL" ;
--------------------------------------------------------
--  Constraints for Table MGO_SEMAPHORE
--------------------------------------------------------

  ALTER TABLE "MGBO"."MGO_SEMAPHORE" MODIFY ("SEM_TYPE" NOT NULL ENABLE);
  ALTER TABLE "MGBO"."MGO_SEMAPHORE" MODIFY ("SEM_STATUS" NOT NULL ENABLE);
