DROP TABLE MGO_ACTIONS;

PROMPT CREATING TABLE 'MGO_ACTIONS';
CREATE TABLE MGBO.MGO_ACTIONS (
  ACT_ACTION_COD     NUMBER(6)
    CONSTRAINT MGO_ACT_ACT_ACTION_COD_NN NOT NULL,
  FUN_FUNCTION_COD   NUMBER(6)
    CONSTRAINT MGO_ACT_FUN_FUNCTION_COD_NN NOT NULL,
  ACT_NAME           VARCHAR2(50)
    CONSTRAINT MGO_ACT_ACT_NAME_NN NOT NULL,
  ACT_URL            VARCHAR2(1000),
  ACT_STAT           VARCHAR2(2)
)
TABLESPACE MGBO_TBL;

PROMPT CREATING CHECK CONSTRAINT ON 'MGO_ACTIONS';

ALTER TABLE MGBO.MGO_ACTIONS
  ADD CONSTRAINT CK_MGO_ACTIONS_ACT_STAT CHECK ( ACT_STAT IN (
    'A'
  ) );

COMMENT ON COLUMN MGBO.MGO_ACTIONS.ACT_ACTION_COD IS
  'Codcie tabella MGO_ACTIONS';

COMMENT ON COLUMN MGBO.MGO_ACTIONS.FUN_FUNCTION_COD IS
  'codice funzione';

COMMENT ON COLUMN MGBO.MGO_ACTIONS.ACT_NAME IS
  'Nome azione';

COMMENT ON COLUMN MGBO.MGO_ACTIONS.ACT_URL IS
  'url api';

CREATE UNIQUE INDEX MGBO.MGO_ACT_ACT_ACTION_COD_IDX ON
  MGBO.MGO_ACTIONS (
    ACT_ACTION_COD
  ASC );

CREATE INDEX MGBO.MGO_ACT_FUN_FUNCTION_COD_IDX ON
  MGBO.MGO_ACTIONS (
    FUN_FUNCTION_COD
  ASC );

PROMPT CREATING PRIMARY KEY ON 'MGO_ACTIONS';

ALTER TABLE MGBO.MGO_ACTIONS ADD CONSTRAINT MGO_ACTIONS_PK PRIMARY KEY ( ACT_ACTION_COD );

PROMPT CREATING FOREIGN KEY ON 'MGO_ACTIONS';

ALTER TABLE MGBO.MGO_ACTIONS
  ADD CONSTRAINT MGO_ACT_MGO_FUN_FK FOREIGN KEY ( FUN_FUNCTION_COD )
    REFERENCES MGBO.MGO_FUNCTIONS ( FUN_FUNCTION_COD );