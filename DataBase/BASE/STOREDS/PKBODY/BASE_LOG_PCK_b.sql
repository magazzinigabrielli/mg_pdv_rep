CREATE OR REPLACE PACKAGE BODY FRAME."BASE_LOG_PCK" AS
  --
  /******************************************************************************
   NAME:       BASE_LOG_PCK
   PURPOSE:    Package di logging

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        12/02/2019           r.doti       1. Created this package.
   ******************************************************************************/
  --
  /**
  * Package di appoggio per raggruppare tutte le procedure e funzioni sviluppate
  * per la gestione dei WKF
  * @headcom
  */
  -- COSTANTI A LIVELLO PACKAGE
  kNomePackage       CONSTANT VARCHAR2(30) := 'BASE_LOG_PCK';
  --
  SessionID  BASE_LOG.LOG_ID_SESSIONE%TYPE := NULL;
  WsID       BASE_LOG.LOG_PKG%TYPE      := NULL;
  LvlTrace   BASE_LOG_CONFIG.CNF_TRACE_LVL%TYPE := NULL;
  NumRiga    BASE_LOG.LOG_NUM_RIGA%TYPE     := 0;
  TRACE_ON   BOOLEAN := TRUE;

  ----------------------------------------------------------------------------
 PROCEDURE CreaSessionId
 IS
 BEGIN
   --
   -- RECUPERO DA SEQUENCE L'ID DELLA SESSIONE DEL LOG
   SELECT BASE_LOG_ID_SEQ.NEXTVAL INTO SessionID FROM DUAL;
     --
     -- riazzero il Numenro di Riga
     NumRiga := 0;
     --
 EXCEPTION
   WHEN OTHERS THEN
       RAISE;
  END CreaSessionId;
  ----------------------------------------------------------------------------
 FUNCTION CreaSessionId
 RETURN BASE_LOG.LOG_ID_SESSIONE%TYPE
 IS
   vSessionID  BASE_LOG.LOG_ID_SESSIONE%TYPE;
 BEGIN
     -- RECUPERO ID UNIVOCO DELLA TRANSAZIONE
     --vSessionID := DBMS_TRANSACTION.LOCAL_TRANSACTION_ID(TRUE);
     --
   -- RECUPERO DA SEQUENCE
   SELECT BASE_LOG_SESSION_SEQ.NEXTVAL INTO vSessionID FROM DUAL;
     --
     -- riazzero il Numenro di Riga
     IF( vSessionID <> SessionID ) THEN
       NumRiga := 0;
     END IF;
     --
    RETURN vSessionID;
     --
 EXCEPTION
   WHEN OTHERS THEN
       RAISE;
  END CreaSessionId;
  ----------------------------------------------------------------------------
 -- =================================================
 -- Restituisce una session id da utilizzare
 -- per la scrittura dei messaggi di log
 --
 -- Return: session ID
 -- =================================================
 FUNCTION GetSessionId
 RETURN BASE_LOG.LOG_ID_SESSIONE%TYPE
 IS
 BEGIN
   IF (SessionID IS NULL) THEN
     SessionID := CreaSessionId;
   END IF;
   RETURN SessionID;
 EXCEPTION
   WHEN OTHERS THEN
       RAISE;
  END GetSessionId;
  ----------------------------------------------------------------------------
 -- ===================================================================
  -- Procedura per scrittura messaggi di log in tabella EDW_LOG
 --
 -- ===================================================================
 PROCEDURE WriteLog(
   pLOG_PKG           IN BASE_LOG.LOG_PKG%TYPE,
   pLOG_MSG           IN BASE_LOG.LOG_MSG%TYPE,
   pLOG_TIPO          IN BASE_LOG.LOG_TIPO%TYPE
 )
 IS
 -- Autonomous Transaction is an Oracle8 feature which allows
 -- this procedure to create and commit a transaction even if
 -- the invoking transaction is rolled back. The procedure must
 -- execute a COMMIT or ROLLBACK before returning control to the
 -- invoking procedure. The invoking transaction is not affected
 -- by this transaction.
 --
 PRAGMA AUTONOMOUS_TRANSACTION;
   --
   vPCK_NAME  VARCHAR2(255);
   vPOS       NUMBER;
   --
 BEGIN
    --
    IF TRACE_ON THEN -- verifico se scrittura log abilitata (parametro del pck_log)
      --
      -- recupero il nome del package
      vPOS := INSTR(pLOG_PKG,'.');
      vPCK_NAME := SUBSTR(pLOG_PKG,0,vPOS-1);
      --
      DBMS_OUTPUT.PUT_LINE('vPCK_NAME:'||vPCK_NAME);
      --
      BEGIN
        -- verifica abilitazione al tracciamento per il package
       SELECT CNF_TRACE_LVL
         INTO LvlTrace
         FROM BASE_LOG_CONFIG
        WHERE CNF_PKG = vPCK_NAME;
         --
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          LvlTrace := NULL;
      END;
      --
      --
      IF LvlTrace IS NULL OR LvlTrace = -1 THEN  -- non ho tracciamento abilitato per il package
        --
        BEGIN
          -- verifica abilitazione al tracciamento per funzione del package
          -- (solo se tracciamento package non abilitato)
        SELECT CNF_TRACE_LVL
          INTO LvlTrace
          FROM BASE_LOG_CONFIG
         WHERE CNF_PKG = pLOG_PKG;
           --
        EXCEPTION
          WHEN NO_DATA_FOUND THEN
            LvlTrace := NULL;
        END;
        --
      END IF;
      --
      IF  LvlTrace IS NOT NULL THEN -- ho tracciamento abilitato per il package o package.funzione
        --
      IF (
         (LvlTrace=1 AND  pLOG_TIPO ='E') OR
         (LvlTrace=2 AND (pLOG_TIPO ='E'  OR pLOG_TIPO ='W')) OR
         (LvlTrace=3 AND (pLOG_TIPO ='E'  OR pLOG_TIPO ='W'   OR  pLOG_TIPO ='I')) OR
         (LvlTrace=4 AND (pLOG_TIPO ='E'  OR pLOG_TIPO ='W'   OR  pLOG_TIPO ='I'   OR pLOG_TIPO ='D'))
         ) THEN
             --
           NumRiga :=NumRiga +1;
             --
           INSERT INTO BASE_LOG(
                   LOG_ID_SESSIONE, 
                   LOG_PKG, 
                   LOG_MSG,
                   LOG_TIPO,
                   LOG_NUM_RIGA)
           VALUES (GetSessionId,
                   pLOG_PKG,
                   SUBSTR(pLOG_MSG,0,4000) ,
                   pLOG_TIPO,
                   NumRiga);
             --
           COMMIT;
             --
      END IF;
        --
      END IF;
      --
    END IF;
    --
  EXCEPTION
   WHEN NO_DATA_FOUND THEN
    -- NESSUN LOG ABILITATO
   NULL;
  WHEN OTHERS THEN
   RAISE;
  END WriteLog;
  --
END BASE_LOG_PCK;
/
