BEGIN
  SYS.DBMS_SCHEDULER.DROP_SCHEDULE
    (schedule_name  => 'ORACLEADMIN.OGNI_DOMENICA_ORE_21');
END;
/

BEGIN
  SYS.DBMS_SCHEDULER.CREATE_SCHEDULE
    (
      schedule_name    => 'ORACLEADMIN.OGNI_DOMENICA_ORE_21'
     ,start_date       => TO_TIMESTAMP_TZ('2019/02/15 14:56:40.000000 Europe/Berlin','yyyy/mm/dd hh24:mi:ss.ff tzr')
     ,repeat_interval  => 'FREQ=WEEKLY;BYTIME=210000;BYDAY=SUN'
     ,end_date         => NULL
     ,comments         => 'Runtime:Ore 21 ogni Domenica'
    );
END;
/
--
BEGIN
  DBMS_SCHEDULER.DROP_PROGRAM
    (program_name          => 'ORACLEADMIN.PRG_CLEAN_TABLE');
END;
/
BEGIN
  SYS.DBMS_SCHEDULER.CREATE_PROGRAM
    (
      program_name         => 'ORACLEADMIN.PRG_CLEAN_TABLE'
     ,program_type         => 'STORED_PROCEDURE'
     ,program_action       => 'ORACLEADMIN.CRSMG_CLEAN_PCK.CLEAN_TABLE_JB'
     ,number_of_arguments  => 0
     ,enabled              => FALSE
     ,comments             => 'Programma per la puliza dei dati annulati logicamente'
    );

  SYS.DBMS_SCHEDULER.ENABLE
    (name                  => 'ORACLEADMIN.PRG_CLEAN_TABLE');
END;
/
--
BEGIN
  SYS.DBMS_SCHEDULER.DROP_JOB
    (job_name  => 'ORACLEADMIN.JOB_CLEAN_TABLES');
END;
/

BEGIN
  SYS.DBMS_SCHEDULER.CREATE_JOB
    (
       job_name        => 'ORACLEADMIN.JOB_CLEAN_TABLES'
      ,schedule_name   => 'ORACLEADMIN.OGNI_DOMENICA_ORE_21'
      ,program_name    => 'ORACLEADMIN.PRG_CLEAN_TABLE'
      ,comments        => 'Job per la pulizia dei dati annullati logicamente '
    );
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE
    ( name      => 'ORACLEADMIN.JOB_CLEAN_TABLES'
     ,attribute => 'RESTARTABLE'
     ,value     => FALSE);
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE
    ( name      => 'ORACLEADMIN.JOB_CLEAN_TABLES'
     ,attribute => 'LOGGING_LEVEL'
     ,value     => SYS.DBMS_SCHEDULER.LOGGING_OFF);
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE_NULL
    ( name      => 'ORACLEADMIN.JOB_CLEAN_TABLES'
     ,attribute => 'MAX_FAILURES');
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE_NULL
    ( name      => 'ORACLEADMIN.JOB_CLEAN_TABLES'
     ,attribute => 'MAX_RUNS');
  BEGIN
    SYS.DBMS_SCHEDULER.SET_ATTRIBUTE
      ( name      => 'ORACLEADMIN.JOB_CLEAN_TABLES'
       ,attribute => 'STOP_ON_WINDOW_CLOSE'
       ,value     => FALSE);
  EXCEPTION
    -- could fail if program is of type EXECUTABLE...
    WHEN OTHERS THEN
      NULL;
  END;
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE
    ( name      => 'ORACLEADMIN.JOB_CLEAN_TABLES'
     ,attribute => 'JOB_PRIORITY'
     ,value     => 3);
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE_NULL
    ( name      => 'ORACLEADMIN.JOB_CLEAN_TABLES'
     ,attribute => 'SCHEDULE_LIMIT');
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE
    ( name      => 'ORACLEADMIN.JOB_CLEAN_TABLES'
     ,attribute => 'AUTO_DROP'
     ,value     => FALSE);

  SYS.DBMS_SCHEDULER.ENABLE
    (name                  => 'ORACLEADMIN.JOB_CLEAN_TABLES');
END;
/

