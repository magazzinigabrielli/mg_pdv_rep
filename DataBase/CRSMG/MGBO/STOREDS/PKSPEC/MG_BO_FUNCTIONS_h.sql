CREATE OR REPLACE PACKAGE MGBO.MG_BO_FUNCTIONS AS 
  /******************************************************************************
   NAME:       MG_BO_FUNCTIONS
   PURPOSE:    Package di gestione delle funzioni richieste da BO

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        28/01/2021           r.doti   1. Created this package.
   ******************************************************************************/
  --
  /**
  * RUN_FUNCTION(): procedura di esecuzione delle funzioni
  *
  * @param po_err_severita Gravita' errore: I=Info A=Avviso B=Bloccante F=Fatale
  * @param po_err_codice Codice errore
  * @param po_messaggio Messaggio di errore
  
  */
  PROCEDURE RUN_FUNCTION(
      PO_ERR_SEVERITA       IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE         IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO          IN OUT NOCOPY VARCHAR2,
      P_OPEID               IN     VARCHAR2,
      P_RUOLOID             IN     VARCHAR2,
      P_DECORRENZA          IN     TIMESTAMP,
      P_PDVCOD              IN     VARCHAR2,
      P_FUNZIONECOD         IN     VARCHAR2,
      P_VALUE               IN     VARCHAR2,
      PO_RESULT                OUT VARCHAR2
  );
PROCEDURE UPDATE_ORDER_OUTPUT(
    PO_ERR_SEVERITA       IN OUT NOCOPY VARCHAR2,
    PO_ERR_CODICE         IN OUT NOCOPY VARCHAR2,
    PO_MESSAGGIO          IN OUT NOCOPY VARCHAR2,
    P_ORD_ID            IN            NUMBER, 
    P_ORD_OUTPUT        IN            VARCHAR2
  );
  
  PROCEDURE UPDATE_FISHPRICES_VAT(
    PO_ERR_SEVERITA       IN OUT NOCOPY VARCHAR2,
    PO_ERR_CODICE         IN OUT NOCOPY VARCHAR2,
    PO_MESSAGGIO          IN OUT NOCOPY VARCHAR2    
  );
  
  PROCEDURE TRUNCATE_TABLE(
    PO_ERR_SEVERITA       IN OUT NOCOPY VARCHAR2,
    PO_ERR_CODICE         IN OUT NOCOPY VARCHAR2,
    PO_MESSAGGIO          IN OUT NOCOPY VARCHAR2,
    P_TABLE               IN            VARCHAR2
  );
  
  PROCEDURE UPDATE_TABLE_STATUS(
    PO_ERR_SEVERITA       IN OUT NOCOPY VARCHAR2,
    PO_ERR_CODICE         IN OUT NOCOPY VARCHAR2,
    PO_MESSAGGIO          IN OUT NOCOPY VARCHAR2,
    P_TABLE               IN            VARCHAR2,
    P_OLDSTATUS           IN            VARCHAR2,
    P_NEWSTATUS           IN            VARCHAR2,
    P_COLUMN              IN            VARCHAR2
  );
END MG_BO_FUNCTIONS;
/
