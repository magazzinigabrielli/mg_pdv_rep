CREATE OR REPLACE PACKAGE BODY MGBO.MG_BO_FUNCTIONS AS
--
  /******************************************************************************
   NAME:       MG_BO_FUNCTIONS
   PURPOSE:    Package di gestione delle funzioni richieste da BO

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        28/01/2021           r.doti   1. Created this package.
   ******************************************************************************/
  --
  kFlagAnn           CONSTANT VARCHAR2(1)   := 'A';
  kFlagS             CONSTANT VARCHAR2(1)   := 'S';
  kFlagN             CONSTANT VARCHAR2(1)   := 'N';
  --kGetInfoArt        CONSTANT VARCHAR2(5)   := 'GetInfoArt';
  kStatoKO           CONSTANT VARCHAR2(5)   := 'KO';
  kStatoOK           CONSTANT VARCHAR2(5)   := 'OK';
  vAccapo            VARCHAR2(10)           := CHR(10);
  --
  kFunInfoArt        CONSTANT NUMBER        := 1; --GET_INFO_ARTICOLO  
  
  --
  /**
  * GET_INFO_ART(): procedura che recupera la descrizone articolo dal barcode fornito in input
  *
  * @param po_err_severita Gravita' errore: I=Info A=Avviso B=Bloccante F=Fatale
  * @param po_err_codice Codice errore
  * @param po_messaggio Messaggio di errore 
  
  */
  PROCEDURE GET_INFO_ART(
      PO_ERR_SEVERITA       IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE         IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO          IN OUT NOCOPY VARCHAR2,
      P_OPEID               IN     VARCHAR2,
      P_RUOLOID             IN     VARCHAR2,
      P_DECORRENZA          IN     TIMESTAMP,
      P_PDVCOD              IN     VARCHAR2,
      P_BARCODE             IN     VARCHAR2,
      PO_RESULT                OUT VARCHAR2
  )
  IS
    --
    kNomePackage        CONSTANT VARCHAR2(30) := 'MG_BO_FUNCTIONS';
    vProcName           VARCHAR2(100);
    vPasso              VARCHAR2(4000);
    exDatiObbl          EXCEPTION;
    vInput              VARCHAR2(100);
	--
  BEGIN
    --
    vProcName := 'GET_INFO_ART';
    --
    SELECT TO_NUMBER(FO_ITEM_CODE)||' - ' || ITEM_DESC
      INTO PO_RESULT
      FROM VSBO.BO_ITEM IT,
           VSBO.BO_FO_BARCODE BC
     WHERE BC.BARCODE = P_BARCODE
       AND BC.STORE_ID = P_PDVCOD
       AND BC.ITEM_ID = IT.ITEM_ID;
    --
  EXCEPTION
    WHEN OTHERS THEN
      PO_ERR_SEVERITA := 'F';
      PO_ERR_CODICE   := SQLCODE;
      PO_MESSAGGIO    := kNomePackage||'.'||vProcName||'['||vPasso||']'|| SQLERRM;
      BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_MESSAGGIO, BASE_LOG_PCK.Errore);
  END GET_INFO_ART;
  --
  /**
  * RUN_FUNCTION(): procedura di esecuzione delle funzioni
  *
  * @param po_err_severita Gravita' errore: I=Info A=Avviso B=Bloccante F=Fatale
  * @param po_err_codice Codice errore
  * @param po_messaggio Messaggio di errore
  
  */
  PROCEDURE RUN_FUNCTION(
      PO_ERR_SEVERITA       IN OUT NOCOPY VARCHAR2,
      PO_ERR_CODICE         IN OUT NOCOPY VARCHAR2,
      PO_MESSAGGIO          IN OUT NOCOPY VARCHAR2,
      P_OPEID               IN     VARCHAR2,
      P_RUOLOID             IN     VARCHAR2,
      P_DECORRENZA          IN     TIMESTAMP,
      P_PDVCOD              IN     VARCHAR2,
      P_FUNZIONECOD         IN     VARCHAR2,
      P_VALUE               IN     VARCHAR2,
      PO_RESULT                OUT VARCHAR2
  )
  IS
    --
    kNomePackage        CONSTANT VARCHAR2(30) := 'MG_BO_FUNCTIONS';
    vProcName           VARCHAR2(30);
    vPasso              VARCHAR2(100);
    exDatiObbl          EXCEPTION;
    vInput              VARCHAR2(100);
    vResult             VARCHAR2(4000);
	--
  BEGIN
    --1
    vProcName := 'RUN_FUNCTION';
    --
    vPasso := 'INIT';
    BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, 'IN', BASE_LOG_PCK.Info);
    BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, '' , vPasso);
    --
    -- Inizializzazione
    vPasso := 'CHECK_PAR_INPUT';
    --
    IF P_OPEID       IS NULL THEN vInput := 'P_OPEID'; RAISE exDatiObbl; END IF;
    IF P_PDVCOD      IS NULL THEN vInput := 'P_PDVCOD'; RAISE exDatiObbl; END IF;
    IF P_RUOLOID     IS NULL THEN vInput := 'P_RUOLOID'; RAISE exDatiObbl; END IF;
    IF P_DECORRENZA  IS NULL THEN vInput := 'P_DECORRENZA'; RAISE exDatiObbl; END IF;
    IF P_FUNZIONECOD IS NULL THEN vInput := 'P_FUNZIONECOD'; RAISE exDatiObbl; END IF;
    IF P_VALUE       IS NULL THEN vInput := 'P_VALUE'; RAISE exDatiObbl; END IF;  
    --
    BASE_LOG_PCK.WriteLog(kNomePackage, vProcName, BASE_LOG_PCK.Info);
    --
    IF P_FUNZIONECOD = kFunInfoArt THEN
    --
      GET_INFO_ART(
        PO_ERR_SEVERITA       => PO_ERR_SEVERITA, 
        PO_ERR_CODICE         => PO_ERR_CODICE  ,
        PO_MESSAGGIO          => PO_MESSAGGIO   , 
        P_OPEID               => P_OPEID        , 
        P_RUOLOID             => P_RUOLOID      , 
        P_DECORRENZA          => P_DECORRENZA   , 
        P_PDVCOD              => P_PDVCOD       ,
        P_BARCODE             => P_VALUE       , 
        PO_RESULT             => vResult
      );
    END IF;
    PO_RESULT := vResult;
    --
  EXCEPTION
    WHEN exDatiObbl THEN
       PO_ERR_SEVERITA := 'E';
       PO_ERR_CODICE   := SQLCODE;
       PO_MESSAGGIO    :=  'ERRORE: IL PARAMETRO '|| vInput ||' E'' OBBLIGATORIO.';
       BASE_TRACE_PCK.TRACE(kNomePackage,vProcName, PO_MESSAGGIO , vPasso);
    WHEN OTHERS THEN
      PO_ERR_SEVERITA := 'F';
      PO_ERR_CODICE   := SQLCODE;
      PO_MESSAGGIO    := kNomePackage||'.'||vProcName||'['||vPasso||']'|| SQLERRM;
      BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_MESSAGGIO, BASE_LOG_PCK.Errore);
  END RUN_FUNCTION;

    /*
 procedura che aggiorna solo l'output inviato al BO, dopo che l'ordine viene
 memorizzato in MGO_ORDERS_RECEIVED
 */
  
  PROCEDURE UPDATE_ORDER_OUTPUT(
    PO_ERR_SEVERITA       IN OUT NOCOPY VARCHAR2,
    PO_ERR_CODICE         IN OUT NOCOPY VARCHAR2,
    PO_MESSAGGIO          IN OUT NOCOPY VARCHAR2,
    P_ORD_ID            IN            NUMBER, 
    P_ORD_OUTPUT        IN            VARCHAR2
  )
  IS
    kNomePackage        CONSTANT VARCHAR2(30) := 'MG_BO_FUNCTIONS';
    vProcName           VARCHAR2(100);
    vPasso              VARCHAR2(4000);
    exDatiObbl          EXCEPTION;
    vInput              VARCHAR2(100);
    
    BEGIN
    --
    vProcName := 'UPDATE_ORDER_OUTPUT';
    --
    UPDATE MGO_ORDERS_RECEIVED 
    SET ORD_OUTPUT = P_ORD_OUTPUT, 
        ORD_UPDATE_DATE=SYSDATE
    WHERE ORD_ORDER_ID = P_ORD_ID;
       
    --
  EXCEPTION
    WHEN OTHERS THEN
      PO_ERR_SEVERITA := 'F';
      PO_ERR_CODICE   := SQLCODE;
      PO_MESSAGGIO    := kNomePackage||'.'||vProcName||'['||vPasso||']'|| SQLERRM;
      BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_MESSAGGIO, BASE_LOG_PCK.Errore);
  END UPDATE_ORDER_OUTPUT;
  
  
  PROCEDURE UPDATE_FISHPRICES_VAT(
    PO_ERR_SEVERITA       IN OUT NOCOPY VARCHAR2,
    PO_ERR_CODICE         IN OUT NOCOPY VARCHAR2,
    PO_MESSAGGIO          IN OUT NOCOPY VARCHAR2    
  )
   IS
    kNomePackage        CONSTANT VARCHAR2(30) := 'MG_BO_FUNCTIONS';
    vProcName           VARCHAR2(100);
    vPasso              VARCHAR2(4000);
    exDatiObbl          EXCEPTION;
    vInput              VARCHAR2(100);
    
    BEGIN
    --
    vProcName := 'UPDATE_FISHPRICES_VAT';
    --

    UPDATE MGO_FISH_PRICES 
    SET FIP_VATCODE = (select distinct VAC_EXTVAT from mgo_vatcodes where vac_item=fip_item),
    FIP_UPDATEDATE=SYSDATE 
    where fip_status='ME';
          
    --
  EXCEPTION
    WHEN OTHERS THEN
      PO_ERR_SEVERITA := 'F';
      PO_ERR_CODICE   := SQLCODE;
      PO_MESSAGGIO    := kNomePackage||'.'||vProcName||'['||vPasso||']'|| SQLERRM;
      BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_MESSAGGIO, BASE_LOG_PCK.Errore);
  END UPDATE_FISHPRICES_VAT;
  
  
   PROCEDURE TRUNCATE_TABLE(
    PO_ERR_SEVERITA       IN OUT NOCOPY VARCHAR2,
    PO_ERR_CODICE         IN OUT NOCOPY VARCHAR2,
    PO_MESSAGGIO          IN OUT NOCOPY VARCHAR2,
    P_TABLE               IN            VARCHAR2
  )
  IS
    kNomePackage        CONSTANT VARCHAR2(30) := 'MG_BO_FUNCTIONS';
    vProcName           VARCHAR2(100);
    vPasso              VARCHAR2(4000);
    exDatiObbl          EXCEPTION;
    vInput              VARCHAR2(100);
    
    BEGIN
    --
    vProcName := 'TRUNCATE_TABLE';
    --
    execute immediate 'truncate table ' || P_TABLE;
    --
  EXCEPTION
    WHEN OTHERS THEN
      PO_ERR_SEVERITA := 'F';
      PO_ERR_CODICE   := SQLCODE;
      PO_MESSAGGIO    := kNomePackage||'.'||vProcName||'['||vPasso||']'|| SQLERRM;
      BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_MESSAGGIO, BASE_LOG_PCK.Errore);
  END TRUNCATE_TABLE;

  PROCEDURE UPDATE_TABLE_STATUS(
    PO_ERR_SEVERITA       IN OUT NOCOPY VARCHAR2,
    PO_ERR_CODICE         IN OUT NOCOPY VARCHAR2,
    PO_MESSAGGIO          IN OUT NOCOPY VARCHAR2,
    P_TABLE               IN            VARCHAR2,
    P_OLDSTATUS           IN            VARCHAR2,
    P_NEWSTATUS           IN            VARCHAR2, 
    P_COLUMN              IN            VARCHAR2
  )IS
    kNomePackage        CONSTANT VARCHAR2(30) := 'MG_BO_FUNCTIONS';
    vProcName           VARCHAR2(100);
    vPasso              VARCHAR2(4000);
    exDatiObbl          EXCEPTION;
    vInput              VARCHAR2(100);
    newStatus           varchar(4);
    oldStatus           varchar(4);
    
    
    
    BEGIN
    --
    vProcName := 'UPDATE_TABLE_STATUS';
    --
    newStatus := '''' || P_NEWSTATUS || '''';
    oldStatus := '''' || P_OLDSTATUS || '''';
   execute immediate 'update ' || P_TABLE || ' SET ' || P_COLUMN || ' =  ' || newStatus || ' WHERE '
   || P_COLUMN || ' = ' || oldStatus;
    --
  EXCEPTION
    WHEN OTHERS THEN
      PO_ERR_SEVERITA := 'F';
      PO_ERR_CODICE   := SQLCODE;
      PO_MESSAGGIO    := kNomePackage||'.'||vProcName||'['||vPasso||']'|| SQLERRM;
      BASE_LOG_PCK.WriteLog(kNomePackage||'.'||vProcName, PO_MESSAGGIO, BASE_LOG_PCK.Errore);
  END UPDATE_TABLE_STATUS;

END MG_BO_FUNCTIONS;
/
